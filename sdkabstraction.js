import { LogService, MatrixClient, MatrixError } from "matrix-bot-sdk";

export class Sdk extends MatrixClient {
  async getUserPowerLevel(userId, roomId) {
    const m_room_power_levels = await this.getRoomStateEvent(
      roomId,
      "m.room.power_levels",
      "",
    );
    return this.getUserPowerLevelFromState(userId, roomId, m_room_power_levels);
  }
  getUserPowerLevelFromState(userId, roomId, m_room_power_levels) {
    if (m_room_power_levels === null) {
      if (this.getRoomStateEvent(roomId, "m.room.create").creator === userId) {
        return 100;
      }
      return 0;
    }
    if (m_room_power_levels?.users?.[userId] != undefined) {
      return m_room_power_levels.users[userId];
    }
    if (m_room_power_levels.users_default !== undefined) {
      return m_room_power_levels.users_default;
    }
    return 0;
  }

  /**
   * @param roomId — the room ID
   * @param type — the event type
   * @param stateKey — the state key
   * @returns — resolves to the state event
   */
  async getRoomStateEvent(room, type, state) {
    try {
      return await this.executeFunctionWithTimeout(
        super.getRoomStateEvent,
        room,
        type,
        state,
      );
    } catch (e) {
      if (e.statusCode === 404) {
        return null;
      }
      throw e;
    }
  }
  /**
   * @param roomId — The room ID to get members in.
   * @param batchToken — The point in time to get members at (or null for 'now')
   * @param membership — The membership kinds to search for.
   * @param notMembership — The membership kinds to not search for.
   * @returns — Resolves to the membership events of the users in the room.
   * @see — getRoomMembersByMembership
   * @see — getRoomMembersWithoutMembership
   * @see — getAllRoomMembers
   */
  async getRoomMembers(roomId, batchToken, membership, notMembership) {
    return await this.executeFunctionWithTimeout(
      super.getRoomMembers,
      roomId,
      batchToken,
      membership,
      notMembership,
    );
  }
  /**
   * @param userId — the user ID to invite
   * @param roomId — the room ID to invite the user to
   * @returns — resolves when completed
   */
  async inviteUser(userId, roomId) {
    return await this.executeFunctionWithTimeout(
      super.inviteUser,
      userId,
      roomId,
    );
  }

  /**
   * @param roomId — the room ID to send the event to
   * @param type — the event type to send
   * @param stateKey — the state key to send, should not be null
   * @param content — the event body to send
   * @returns — resolves to the event ID that represents the message
   */
  async sendStateEvent(roomId, type, stateKey, content) {
    return await this.executeFunctionWithTimeout(
      super.sendStateEvent,
      roomId,
      type,
      stateKey,
      content,
    );
  }

  /**
   * @param roomId — the room ID to send the notice to
   * @param text — the text to send
   * @returns — resolves to the event ID that represents the message
   */
  async sendNotice(roomId, text) {
    return await this.executeFunctionWithTimeout(
      super.sendNotice,
      roomId,
      text,
    );
  }

  /**
   * @param {Function} fun
   * @param  {...any} params
   */
  async executeFunctionWithTimeout(fun, ...params) {
    try {
      return await fun.apply(this, params);
    } catch (e) {
      if (e instanceof MatrixError && e.retryAfterMs != undefined) {
        LogService.warn(
          "sdkabstraction",
          "Will retry call in " + e.retryAfterMs + "ms\n",
          e,
        );
        return await new Promise(async (resolve) => {
          await new Promise((res) => setTimeout(res, e.retryAfterMs));
          resolve(await this.executeFunctionWithTimeout(fun, ...params));
        });
      }
      e.params = params;
      LogService.error("sdkabstraction", "Not a Retry-After-Error!\n", e);

      throw e;
    }
  }
}
