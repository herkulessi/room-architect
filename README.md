# room-architect

A Matrix bot for setting and keeping the same power levels across a bunch of rooms

# Goals

The goal of this project is to bring Discord-Style permission management to Matrix. Since that is not entirely possible (you can't deny a user a permission a lower power level has in Matrix) this project will mostly focus on grouping users into groups, which can be given a collective powerlevel in a room (or group of rooms) if so desired. Eventually this bot is supposed to allow permission changes via messaging and while running, but right now it ignores all message events.

## What it currently works

- [x] Parsing the Config
  - [x] Normalising user groups away
  - [x] Normalising room groups away
- [ ] Let (Bot-)Admins change the config
  - [ ] Changing via DMs
  - [ ] Support for encrypted DMs
  - [ ] Persisting Changes
- [ ] Setting power levels
  - [x] Setting per-user or per-group power levels
  - [ ] Set required/default power levels in a room
- [ ] Inheritance
  - [ ] Rooms inherit Powerlevels from the space

## Ideas for future Features

- Web-UI
  - I don't know if I want to do this
  - The bot doesn't have to be reachable from the Internet right now, maybe that is for the best...
  - Can be accomplished with a second bot sending this one DMs.
- Support for mass-inviting every in a room which has a power level set
- Persist manual power level changes to config

## Configuration

All configurations in terms of Permissions are in the JSON-File speicified in the .env file.
A sample config is provided in this repo, but it generally works like this:

```js
{
  "admin": "<MXID or user-group name of Bot admin(s), currently unused>",
  "user-group": {
    "<groupname>": [
      "<mxid or user-group name>",
      "<another mxid or user-group name>"
      // , ...
    ]
  },
  "room-group": {
    // Room aliases are currently NOT supported
    "<groupname>": [
      "<room id or room-group name>",
      "<another room if or room-group name>"
      // , ...
    ]
  },
  "room-power-level-state-event": {
    // this is a normal m.room.power_levels event outside of users
    "<room id or room-group name>": {
      "users": {
        "<mxid or user-group name>": "<power level>",
        "<another mxid or user-group name>": "<power level>"
        // , ...
      }
    },
    "<another room id or room-group name>": {
      "users": {
        "<mxid or user-group name>": "<power level>",
        "<another mxid or user-group name>": "<power level>"
        // , ...
      }
    }
    // , ...
  }
}
```

## Usage

Just copy `.env.example` to `.env`, set the bot token, the path to the config.json and to the botstate.json (where the bot keeps it's state across restarts, it will create the file on its own)

Then run the index.js with (for example)

```sh
node index.js
```
