import * as fs from "fs";
import {
  AutojoinRoomsMixin,
  LogLevel,
  LogService,
  MatrixClient,
  RichConsoleLogger,
  SimpleFsStorageProvider,
} from "matrix-bot-sdk";
import "dotenv/config";
import { Sdk } from "./sdkabstraction.js";

LogService.setLogger(new RichConsoleLogger());
LogService.setLevel(LogLevel.DEBUG);

const config = JSON.parse(fs.readFileSync(process.env.CONFIG_FILE));

function normalizeConfig(config) {
  if (config === undefined || config === null) {
    throw "normalizeConfig: No config supplied";
  }
  if (config["user-group"] === undefined || config["user-group"] === null) {
    LogService.warn(
      "normalizeConfig",
      "Config does not contain the 'user-group' key!",
    );
    config["user-group"] = {};
  }
  if (config["room-group"] === undefined || config["room-group"] === null) {
    LogService.warn(
      "normalizeConfig",
      "Config does not contain the roomgroups key!",
    );
    config["room-group"] = {};
  }
  if (
    config["room-power-level-state-event"] === undefined ||
    config["room-power-level-state-event"] === null
  ) {
    LogService.warn(
      "normalizeConfig",
      "Config does not contain the permissions key!",
    );
    config["room-power-level-state-event"] = {};
  }
  if (typeof config.admin !== "string") {
    LogService.warn(
      "normalizeConfig",
      "Config does not contain an admin, was " + config.admin,
    );
  }

  const normalizedConfig = {
    "user-group": JSON.parse(JSON.stringify(config["user-group"])),
    "room-group": JSON.parse(JSON.stringify(config["room-group"])),
    admins: [config.admin],
    "room-power-level-state-event": {},
  };

  for (const userGroup in normalizedConfig["user-group"]) {
    for (const currentUserGroup in normalizedConfig["user-group"]) {
      for (
        const memberPos in normalizedConfig["user-group"][
          currentUserGroup
        ]
      ) {
        if (
          normalizedConfig["user-group"][currentUserGroup][memberPos] ==
            userGroup
        ) {
          delete normalizedConfig["user-group"][currentUserGroup][memberPos];
          normalizedConfig["user-group"][currentUserGroup].push(
            ...normalizedConfig["user-group"][userGroup],
          );
        }
      }
    }
  }

  for (const group in normalizedConfig["user-group"]) {
    const groupSet = new Set(normalizedConfig["user-group"][group]);
    groupSet.delete(undefined);
    normalizedConfig["user-group"][group] = [...groupSet];
    for (const member of normalizedConfig["user-group"][group]) {
      if (!member.startsWith("@")) {
        throw "Cycle detected! " + member + " is part of a user-group cycle";
      }
    }
  }

  for (const group in normalizedConfig["room-group"]) {
    for (const current in normalizedConfig["room-group"]) {
      for (const memberPos in normalizedConfig["room-group"][current]) {
        if (normalizedConfig["room-group"][current][memberPos] == group) {
          delete normalizedConfig["room-group"][current][memberPos];
          normalizedConfig["room-group"][current].push(
            ...normalizedConfig["room-group"][group],
          );
        }
      }
    }
  }

  for (const roomGroup in normalizedConfig["room-group"]) {
    const roomGroupSet = new Set(normalizedConfig["room-group"][roomGroup]);
    roomGroupSet.delete(undefined);
    normalizedConfig["room-group"][roomGroup] = [...roomGroupSet];
    for (const member of normalizedConfig["room-group"][roomGroup]) {
      if (!member.startsWith("!")) {
        throw "Cycle detected! " + member + " is part of a room-group cycle";
      }
    }
  }

  // Copy Roomname-Permission
  Object.keys(config["room-power-level-state-event"])
    .filter((e) => e.startsWith("!"))
    .forEach((element) => {
      normalizedConfig["room-power-level-state-event"][element] =
        config["room-power-level-state-event"][element];
    });

  // Resolve roomgroup-Permission
  Object.keys(config["room-power-level-state-event"])
    .filter((e) => !e.startsWith("!"))
    .forEach((e) => {
      normalizedConfig["room-group"][e].forEach((element) => {
        normalizedConfig["room-power-level-state-event"][element] =
          config["room-power-level-state-event"][e];
      });
    });

  // Flatten usergroup-permissions
  for (const room in normalizedConfig["room-power-level-state-event"]) {
    for (
      const usergroup in normalizedConfig["room-power-level-state-event"][room]
        .users || {}
    ) {
      if (usergroup.startsWith("@")) {
        continue;
      }
      const powerlevel =
        normalizedConfig["room-power-level-state-event"][room].users[usergroup];
      LogService.trace(
        "normalizeConfig",
        usergroup,
        normalizedConfig["user-group"],
      );
      for (const user of normalizedConfig["user-group"][usergroup]) {
        LogService.trace(
          "normalizeConfig",
          room,
          user,
          powerlevel,
          normalizedConfig["room-power-level-state-event"][room].users[user],
        );
        normalizedConfig["room-power-level-state-event"][room] ??= {};
        normalizedConfig["room-power-level-state-event"][room].users ??= {};
        normalizedConfig["room-power-level-state-event"][room].users[user] ??=
          Number.MIN_SAFE_INTEGER;
        normalizedConfig["room-power-level-state-event"][room].users[user] =
          Math.max(
            powerlevel,
            normalizedConfig["room-power-level-state-event"][room].users[user],
          );
      }
    }
  }
  for (
    const [room, roomperm] of Object.entries(
      normalizedConfig["room-power-level-state-event"],
    )
  ) {
    const users = roomperm.users;
    normalizedConfig["room-power-level-state-event"][room].users = {};
    for (const [user, value] of Object.entries(users)) {
      if (user.startsWith("@")) {
        normalizedConfig["room-power-level-state-event"][room].users[user] =
          value;
      }
    }
  }

  return normalizedConfig;
}
function sendErrorMessage(client, room, message) {
  LogService.trace(
    "sendErrorMessage",
    `Creating error message sender for room ${room} with message ${
      JSON.stringify(
        message,
      )
    }`,
  );
  return (e) => {
    LogService.error(
      "sendErrorMessage",
      `Error occured! Sending the following message to room ${room}:\n${message}${e.toString()}\n${
        JSON.stringify(
          e,
          null,
          4,
        )
      }`,
    );
    client.sendNotice(
      room,
      message + e.toString() + "\n" + JSON.stringify(e, null, 4),
    );
  };
}
function mergeObjects(a, b) {
  let somethingChanged = false;
  for (const key in a) {
    if (b[key] === undefined) continue;
    if (typeof a[key] != typeof b[key]) {
      throw `Could not merge ${key} from ${a} and ${b}: Different types!`;
    } else if (typeof a[key] == "object") {
      somethingChanged = somethingChanged || mergeObjects(a[key], b[key]);
    } else if (typeof a[key] == "number") {
      if (a[key] != b[key]) {
        somethingChanged = true;
        a[key] = b[key];
      }
    } else throw "Can't merge non-numbers!";
  }
  for (const key in b) {
    if (a[key] !== undefined) continue;
    somethingChanged = true;
    a[key] = b[key];
  }
  return somethingChanged;
}
async function assurePermissionsMerge(client, room, config) {
  // get state
  const state = await client.getRoomStateEvent(room, "m.room.power_levels", "");
  if (state === null) {
    // No current power_level state
    const config_clone = JSON.parse(
      JSON.stringify(config["room-power-level-state-event"][room]),
    );
    config_clone.users ??= {};
    config_clone.users[BOT_MXID] = await this.getUserPowerLevelFromState(
      BOT_MXID,
      null,
    );
    await client.sendStateEvent(room, "m.room.power_levels", "", config_clone)
      .catch((e) =>
        LogService.error(
          "assurePermissionsMerge",
          `sendStateEvent ${JSON.stringify(e, null, 2)}`,
        )
      );
    return true;
  }
  state.users ??= {};

  const bot_powerlevel = await client.getUserPowerLevelFromState(
    BOT_MXID,
    room,
    state,
  );
  // Look for necessaiy changes and do them
  const anythingChanged = mergeObjects(
    state,
    config["room-power-level-state-event"][room],
  );
  if (!anythingChanged) {
    LogService.debug(
      "assurePermissions",
      "Nothing changed " + JSON.stringify(state, null, 2),
    );
    return false;
  }
  state.users[BOT_MXID] = bot_powerlevel;
  LogService.debug(
    "assurePermissions",
    "Changed to " + JSON.stringify(state, null, 2),
  );
  await client.sendStateEvent(room, "m.room.power_levels", "", state).catch(
    (e) => {
      LogService.error(
        "assurePermissionsMerge",
        `sendStateEvent ${JSON.stringify(e, null, 2)}`,
      );
    },
  );
  return true;
}
function objectDeepEquals(a, b) {
  if (typeof a !== typeof b) return false;
  if (typeof a !== "object") return a === b;
  let equals = true;
  for (const key in a) {
    equals &&= objectDeepEquals(a[key], b[key]);
  }
  for (const key in b) {
    if (a[key] === undefined) equals = false;
  }
  return equals;
}

async function assurePermissionsReplace(client, room, config) {
  let state = await client.getRoomStateEvent(room, "m.room.power_levels");
  LogService.trace(
    "assurePermissionsReplace",
    "\n",
    JSON.stringify(state),
    "\n",
    JSON.stringify(config["room-power-level-state-event"][room]),
  );
  const botPowerLevel = await client.getUserPowerLevelFromState(
    BOT_MXID,
    room,
    state,
  );
  state ??= {};
  state.users ??= {};
  state.users[BOT_MXID] = botPowerLevel;
  config["room-power-level-state-event"][room].users ??= {};
  config["room-power-level-state-event"][room].users[BOT_MXID] = botPowerLevel;

  if (objectDeepEquals(state, config["room-power-level-state-event"][room])) {
    return false;
  }
  LogService.trace(
    "assurePermissionsReplace",
    "\n",
    JSON.stringify(state),
    "\n",
    JSON.stringify(config["room-power-level-state-event"][room]),
  );
  client.sendStateEvent(
    room,
    "m.room.power_levels",
    "",
    config["room-power-level-state-event"][room],
  ).catch(
    (e) => {
      LogService.error(
        "assurePermissionsReplace",
        `sendStateEvent ${JSON.stringify(e, null, 2)}`,
      );
    },
  );
}

async function assurePermissions(client, room, config) {
  if (
    room == undefined ||
    config["room-power-level-state-event"][room] == undefined
  ) {
    LogService.warn(
      "assurePermissions",
      `Invalid room ${room}, cannot assure Permissions.`,
    );
    return;
  }
  if (process.env.REPLACE_DONT_MERGE) {
    return await assurePermissionsReplace(client, room, config);
  } else {
    return await assurePermissionsMerge(client, room, config);
  }
}
async function autoinvite(client, room, config) {
  const members = {};
  for (const member of await client.getRoomMembers(room)) {
    if (member?.event?.state_key == undefined) {
      continue;
    }
    members[member?.event?.state_key] = member?.event?.content?.membership;
  }
  if (config["auto-join"] == undefined) {
    return;
  }
  for (const tojoin of config["auto-join"]?.[room]) {
    if (members[tojoin] == undefined || members[tojoin] == "knock") {
      await client.inviteUser(tojoin, room);
    }
  }
}

const storage = new SimpleFsStorageProvider(process.env.STATE_FILE);
const client = new Sdk(
  process.env.HOMESERVER_URL,
  process.env.BOT_TOKEN,
  storage,
);
AutojoinRoomsMixin.setupOnClient(client);
client.syncingPresence = "online";

const normalizedConfig = normalizeConfig(config);

client.on("room.join", (e, _v) => {
  LogService.info(
    `Bot joined the following room: '${e}'\n${JSON.stringify(e, null, 2)}`,
  );
  assurePermissions(client, e, normalizedConfig).catch((e) => {
    LogService.error(
      "Error running assurePermissions:\n" + JSON.stringify(e, null, 2),
    );
  });
  LogService.info(`Inviting Users to room '${e}'`);
  autoinvite(client, e, normalizedConfig).catch((e) => {
    LogService.error(
      "Error running AutoInvite:\n" + JSON.stringify(e, null, 2),
    );
  });
});
client.on("room.event", (e, v) => {
  LogService.trace(
    "room.event",
    `Got event in room '${e}'!\n${JSON.stringify(v, null, 2)}`,
  );

  if (v["type"] !== "m.room.power_levels") {
    LogService.trace(
      "room.event",
      "Event was not a 'm.room.power_levels' event",
    );
    return;
  }
  assurePermissions(client, e, normalizedConfig);
});
client.on("room.leave", (e) => {
  LogService.warn(
    "room.leave",
    "Bot left/was kicked from the following room: " +
      JSON.stringify(e, null, 2),
  );
});

await client.start();
var BOT_MXID = (await client.getWhoAmI()).user_id;

LogService.info("main", "Bot started!");
LogService.debug("main", "Iterating over joined rooms");
(await client.getJoinedRooms()).forEach((e) =>
  assurePermissions(client, e, normalizedConfig).catch(
    LogService.error(
      "assurePermissions",
      "Error running AssurePermissions:\n" + JSON.stringify(e, null, 2),
    ),
  )
);
LogService.info("main", "Assured permissions for every joined room");
